public without sharing class ominiChannelActiveusers {
@AuraEnabled
    public static List<User> getOminiChannelActiveUsers(String searchKey) {  
        List<UserServicePresence> currentUserStatuses =  [SELECT UserId,ServicePresenceStatus.DeveloperName  FROM UserServicePresence WHERE ServicePresenceStatus.DeveloperName ='Online'];
        
        Set<Id> availableStatusIds = new Set<Id>();
        
        for(UserServicePresence us :currentUserStatuses){
            availableStatusIds.add(us.UserId);
        }
        List<User> UsersList = new List<User>();
        if(String.isNotBlank(searchKey)) {
            searchKey = '%' + searchKey.trim() + '%';
            
            UsersList = [
                Select Id,Name,Languages_Spoken__c
                From User 
                Where (Name Like : searchKey)
                AND 
                Id IN :availableStatusIds
                Limit 20
            ];
        }
        return UsersList;
    } 
}